package jp.or.codefortoyamacity.livetoday;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wakabayashieisuke on 2016/09/25.
 */

public final class ConfigManager {

    private static final String SHARED_PREFERENCES_NAME = "Config";
    private static final String KEY_IS_STARTED = "isstarted";
    private static final String KEY_HOUR = "hour";
    private static final String KEY_MINUTE = "minute";
    private static final String KEY_PREF = "pref";

    private ConfigManager() {}

    public static void putIsStarted(Context context, boolean isStarted) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = data.edit();
        editor.putBoolean(KEY_IS_STARTED, isStarted);
        editor.apply();
    }

    public static void putHour(Context context, int hour) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = data.edit();
        editor.putInt(KEY_HOUR, hour);
        editor.apply();
    }

    public static void putMinute(Context context, int minute) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = data.edit();
        editor.putInt(KEY_MINUTE, minute);
        editor.apply();
    }

    public static void putPref(Context context, String pref) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = data.edit();
        editor.putString(KEY_PREF, pref);
        editor.apply();
    }

    public static boolean isStarted(Context context) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return data.getBoolean(KEY_IS_STARTED, false);
    }

    public static int getHour(Context context) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return data.getInt(KEY_HOUR, -1);
    }

    public static int getMinute(Context context) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return data.getInt(KEY_MINUTE, -1);
    }

    public static String getPref(Context context) {
        SharedPreferences data = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return data.getString(KEY_PREF, "北海道");
    }
}
