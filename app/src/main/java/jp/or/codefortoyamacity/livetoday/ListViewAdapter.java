package jp.or.codefortoyamacity.livetoday;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * 履歴リスト用Adapter
 * Created by wakabayashieisuke on 2016/07/07.
 */
public class ListViewAdapter extends ArrayAdapter<History> {
    protected LayoutInflater inflater;

    public ListViewAdapter(Context context, ArrayList<History> histories) {
        super(context, R.layout.hisotry_list_cell, histories);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.hisotry_list_cell, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.historyListTitle);
            holder.datetime = (TextView) convertView.findViewById(R.id.historyListDatetime);
            holder.icon = (ImageView) convertView.findViewById(R.id.historyListIcon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final History history = getItem(position);
        holder.title.setText(history.getTitle());
        holder.datetime.setText(history.getDateTime());
        holder.icon.setImageBitmap(history.getIcon());

        return convertView;
    }

    private static class ViewHolder {
        private TextView title;
        private TextView datetime;
        private ImageView icon;
    }
}
