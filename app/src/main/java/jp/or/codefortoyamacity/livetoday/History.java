package jp.or.codefortoyamacity.livetoday;

import android.graphics.Bitmap;

/**
 * Created by wakabayashieisuke on 2016/10/06.
 */

public class History {
    private String title;
    private String dateTime;
    private Bitmap icon;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }
}
