package jp.or.codefortoyamacity.livetoday;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

/**
 * Created by wakabayashieisuke on 2016/09/25.
 */

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    public static final String TAP = "tap";
    public static final String TWEET = "tweet";

    public static final String EXTRA_KEY_TWEET_MESSAGE = "tweet_message";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TAP.equals(action)) {
            String url = "https://ja.wikipedia.org/wiki/飛越地震";

            // ブラウザアプリがインストールされてい無い場合エラー
            if (!isExistsBrowserApp(context, url)) {
                return;
            }

            openBrowserApp(context, url);
        } else if (TWEET.equals(action)) {
            String url = "http://twitter.com/share?text=" + intent.getStringExtra(EXTRA_KEY_TWEET_MESSAGE);
            Intent twitterIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            twitterIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(twitterIntent);
        }
    }

    private void openBrowserApp(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private boolean isExistsBrowserApp(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        return checkImplicitIntent(context, intent);
    }

    private boolean checkImplicitIntent(Context context, Intent intent) {
        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        if (activities.isEmpty()) {
            return false;
        }
        return true;
    }
}
