package jp.or.codefortoyamacity.livetoday;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Snackbar startedSnackbar;
    private Snackbar stoppedSnackbar;

    private ListView historyListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Switch alermSwitch = (Switch) findViewById(R.id.alermSwitch);
        alermSwitch.setChecked(ConfigManager.isStarted(this));
        alermSwitch.setOnCheckedChangeListener(checkedChangeListener);
        setTime();
        setTimePiker();
        ((Spinner) findViewById(R.id.spinner)).setOnItemSelectedListener(new SpinnerSelectedListener());
        setHistoryListView();
    }

    /**
     * 時刻テキストクリックでタイムピッカーが用事されるようにする
     */
    private void setTimePiker() {
        ((TextView) findViewById(R.id.alermTime)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Context context = view.getContext();
                        int hour = ConfigManager.getHour(context);
                        if (hour == -1) {
                            hour = 0;
                        }
                        int minute = ConfigManager.getMinute(context);
                        if (minute == -1) {
                            minute = 0;
                        }
                        TimePickerDialog timePickerDialog = new TimePickerDialog(context
                                , new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                // 内部ストレージに設定した時刻を保持
                                ConfigManager.putHour(getApplicationContext(), hourOfDay);
                                ConfigManager.putMinute(getApplicationContext(), minute);
                                setTime();
                                if (ConfigManager.isStarted(view.getContext())) {
                                    DailyScheduler dailyScheduler = new DailyScheduler(view.getContext());
                                    dailyScheduler.cancel(NotificationService.class, -1);
                                    dailyScheduler.setByTime(NotificationService.class, hourOfDay, minute, -1);
                                    // メッセージ表示
                                    String message = ((TextView) findViewById(R.id.alermTime)).getText() + " に設定しました。";
                                    showStartedSnackBar(message);
                                }
                            }
                        }, hour, minute, false);
                        timePickerDialog.show();
                    }
                }
        );
    }

    private void setTime() {
        String time = "";
        int hour = ConfigManager.getHour(this);
        if (hour == -1) {
            hour = 0;
        }
        time += String.valueOf(hour) + ":";
        int minute = ConfigManager.getMinute(this);
        if (minute == -1) {
            time += "00";
        } else if (minute <= 9) {
            time = time + "0" + String.valueOf(minute);
        } else {
            time += String.valueOf(minute);
        }
        ((TextView) findViewById(R.id.alermTime)).setText(time);
    }

    private CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if (isChecked) {
                start();
            } else {
                stop();
            }
        }

        private void start() {
            Context context = getApplicationContext();
            ConfigManager.putIsStarted(getApplicationContext(), true);
            (new DailyScheduler(context)).setByTime(NotificationService.class
                    , ConfigManager.getHour(context), ConfigManager.getMinute(context), -1);
            // メッセージ表示
            String message = ((TextView) findViewById(R.id.alermTime)).getText() + " に設定しました。";
            showStartedSnackBar(message);
        }

        private void stop() {
            ConfigManager.putIsStarted(getApplicationContext(), false);
            (new DailyScheduler(getApplicationContext())).cancel(NotificationService.class, -1);
            // メッセージを表示
            showStoppedSnackBar();
        }
    };

    /**
     * 都道府県選択スピナーのセレクトリスナ
     */
    public class SpinnerSelectedListener implements AdapterView.OnItemSelectedListener{
        public void onItemSelected(AdapterView parent,View view, int position,long id) {
            // Spinner を取得
            Spinner spinner = (Spinner) parent;
            // 選択されたアイテムのテキストを取得
            String str = spinner.getSelectedItem().toString();
            ConfigManager.putPref(getApplicationContext(), str);
        }

        // 何も選択されなかった時の動作
        public void onNothingSelected(AdapterView parent) {
        }
    }

    /**
     * Startボタンタップ時にスナックバーを表示する。
     * @param message 表示するメッセージ
     */
    private void showStartedSnackBar(String message) {
        startedSnackbar = Snackbar.make(
                findViewById(R.id.activity_main), message, Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startedSnackbar.dismiss();
                    }
                });
        if (stoppedSnackbar != null && stoppedSnackbar.isShown()) {
            stoppedSnackbar.dismiss();
        }
        startedSnackbar.show();
    }

    /**
     * Stopボタンタップ時にスナックバーを表示する。
     */
    private void showStoppedSnackBar() {
        stoppedSnackbar = Snackbar.make(
                findViewById(R.id.activity_main), "通知を解除しました。", Snackbar.LENGTH_LONG)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        stoppedSnackbar.dismiss();
                    }
                });
        if (startedSnackbar != null && startedSnackbar.isShown()) {
            startedSnackbar.dismiss();
        }
        stoppedSnackbar.show();
    }

    public void setHistoryListView() {
        if (historyListView == null) {
            historyListView = (ListView) findViewById(R.id.historyListView);
        }
        // TODO：リスト暫定。内部DBから取得予定
        ArrayList<History> historyList = new ArrayList<>();
        for (int i = 0; i< 10; i++) {
            History history = new History();
            history.setTitle("生存確率" + String.valueOf(i) + "％");
            history.setDateTime("2016/10/0" + i);
            if (i == 0 || i == 5) {
                history.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.inlove));
            } else if (i == 1 || i == 6) {
                history.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.happy));
            } else if (i == 2 || i == 7) {
                history.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.confused));
            } else if (i == 3 || i == 8) {
                history.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ill));
            } else {
                history.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.angel));
            }
            historyList.add(history);
        }
        ListViewAdapter listViewAdapter = new ListViewAdapter(this, historyList);
        historyListView.setAdapter(listViewAdapter);
        historyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), ContentActivity.class);
                startActivity(intent);
            }
        });
    }
}
