package jp.or.codefortoyamacity.livetoday;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 起動時のレシーバ。DailySchedulerにアプリで設定した時間をセットする。
 * Created by wakabayashieisuke on 2016/09/25.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {
    /** TAG */
    private static final String TAG = BootBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive:" + intent.getAction());
        if (!ConfigManager.isStarted(context)) {
            return;
        }
        int hour = ConfigManager.getHour(context);
        if (hour == -1) {
            return;
        }
        int minute = ConfigManager.getMinute(context);
        if (minute == -1) {
            return;
        }
        (new DailyScheduler(context)).setByTime(NotificationService.class,
                hour, minute, -1);
    }
}
