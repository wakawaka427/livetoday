package jp.or.codefortoyamacity.livetoday;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 通知を表示するためのサービスクラス<br/>
 * 指定した時間に達するとDailySchedulerがこのサービスをよびだす。<br/>
 * Serviceが起動するとAPIを呼び出し当日の災害を取得 && 生存確率を取得し画面に通知を表示する。
 */
public class NotificationService extends IntentService {

    private static final String TAG = NotificationService.class.getSimpleName();

    public NotificationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "onHandleIntent");
        // TODO：通知に表示するメッセージ。暫定
        String content = "本日○○地方で大雨による堤防の決壊が発生しました。";
        displayIncomingNotification(content);
        // TODO：エンドポイント確定してから再度実装
//        request();
    }

    /**
     * APIリクエスト
     * TODO：RxAndroidで実装して別クラスに持っていきたい
     */
    private void request() {
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        Network network = new BasicNetwork(new HurlStack());
        RequestQueue queue = new RequestQueue(cache, network);
        queue.start();
//        String url ="http://search-saigai-serch-pnhvcylg7vor7eit2ydbktf5ku.us-east-1.cloudsearch.amazonaws.com/search?";
//        // 現在位置からもっとも近い情報をソートして取得する
//        url += "expr.distance=haversin(35.699789,139.764230,latlon.latitude,latlon.longitude)&sort=distance asc";
        String url = "http://search-saigai-serch-pnhvcylg7vor7eit2ydbktf5ku.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q=0925&expr.distance=haversin(35.699789,139.764230,latlon.latitude,latlon.longitude)&return=distance,title,description,deadcount,date,level&sort=distance asc";
//        String url = "http://search-saigai-serch-pnhvcylg7vor7eit2ydbktf5ku.us-east-1.cloudsearch.amazonaws.com/2013-01-01/search?q=%E5%AF%8C%E5%B1%B1";
        URI javaUriA = URI.create(url);
        JsonObjectRequest jsonObjectRequest = new MyRequest(Request.Method.GET, javaUriA.toASCIIString(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        String content = "本日はとても平和な1日でした。";
                        try {
                            JSONObject jsonObj = response.getJSONObject("hits");
                            JSONArray jsonArray = jsonObj.getJSONArray("hit");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject hit = jsonArray.getJSONObject(i);
                                if (hit == null) {
                                    break;
                                }
                                JSONObject fields = hit.getJSONObject("fields");
                                if (fields == null) {
                                    break;
                                }

                                String dateISO8601 = fromString(fields.getString("date"));
                                content = dateISO8601 + "に" + fields.getString("title") + "が発生しました。";
                                break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        displayIncomingNotification(content);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error
                    }
                });
        queue.add(jsonObjectRequest);
    }

    public String fromString(String string) {
        String[] a = string.split("T");
        return a[0];
    }

    public class MyRequest extends JsonObjectRequest {
        public MyRequest(int method, String url, JSONObject jsonRequest,
                  Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
            super(method, url, jsonRequest, listener, errorListener);
        }
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json; charset=utf-8");
            return headers;
        }
    }

    private String tweetMessage = "";
    /**
     * http://www.flaticon.com/packs/emoticons-5
     */
    private void displayIncomingNotification(String content) {
        NotificationCompat.Builder builder = getBuider(content);
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            displayIncomingNotification(builder);
        } else {
            displayIncomingNotificationICS(builder);
        }

        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // Tickerを更新するため一旦通知を消す
        notificationManager.cancel(0);
        // 通知を表示
        notificationManager.notify(0, builder.build());
    }

    private NotificationCompat.Builder getBuider(String content) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setTicker("本日の生存確率は・・・")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText(content)
                        .setDefaults(Notification.DEFAULT_LIGHTS)
                        .setVibrate(new long[] { 0, 100, 100, 100, 100 })
                        .setLights(Color.RED, 3000, 3000)
                        .setAutoCancel(true);

        Random random = new Random();
        int n = random.nextInt(4);
        if (n == 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.inlove));
            builder.setContentTitle("生存確率100％");
            tweetMessage = "本日の生存確率は100％でした。";
        } else if (n == 1) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.happy));
            builder.setContentTitle("生存確率70％");
            tweetMessage = "本日の生存確率は70％でした。";
        } else if (n == 2) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.confused));
            builder.setContentTitle("生存確率50％");
            tweetMessage = "本日の生存確率は50％でした。";
        } else if (n == 3) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ill));
            builder.setContentTitle("生存確率30％");
            tweetMessage = "本日の生存確率は30％でした。";
        } else if (n == 4) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.angel));
            builder.setContentTitle("生存確率0％");
            tweetMessage = "本日の生存確率は0％でした。";
        }

        return builder;
    }

    private void displayIncomingNotification(NotificationCompat.Builder builder) {
        Intent answerIntent = new Intent(this, NotificationBroadcastReceiver.class);
        answerIntent.setAction(NotificationBroadcastReceiver.TAP);
        PendingIntent answerPendingIntent = PendingIntent.getBroadcast(this, 0, answerIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent answerButtonIntent = new Intent(this, NotificationBroadcastReceiver.class);
        answerButtonIntent.setAction(NotificationBroadcastReceiver.TWEET);
        answerButtonIntent.putExtra(NotificationBroadcastReceiver.EXTRA_KEY_TWEET_MESSAGE, tweetMessage);
        PendingIntent answerButtonPendingIntent = PendingIntent.getBroadcast(this, 0, answerButtonIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // 通知バーにtwitterボタン設定
        builder.addAction(R.drawable.icon_twitter, "つぶやく", answerButtonPendingIntent);
        // ロック画面上に表示された通知では最初からボタンが表示できないため、タップによる応答・スワイプによる削除を有効化しておく
        builder.setContentIntent(answerPendingIntent);

        // Lollipopで通知時にフローティングウィンドウをポップアップさせるためにプライオリティを上げてバイブを設定
        // （プライオリティに加えてサウンドまたはバイブを設定しないとポップアップしない）
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(Notification.PRIORITY_MAX);
        }
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }
    }

    /**
     * Android4系の場合通知バーにボタンを表示することができないため、ボタンなしの通知を生成する。
     * @param builder NotificationCompat.Builder
     */
    private void displayIncomingNotificationICS(NotificationCompat.Builder builder) {
        Intent answerIntent = new Intent(this, NotificationBroadcastReceiver.class);
        answerIntent.setAction(NotificationBroadcastReceiver.TAP);
        PendingIntent answerPendingIntent = PendingIntent.getBroadcast(this, 0, answerIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(answerPendingIntent);
    }
}
